defmodule Ring do
  @doc """
  Spawns a series of n processes where each spawned process spawns a child
  process. So if we spawn 4 processes:
  a -- spawns --> b, a waits for a msg
  b -- spawns --> c, b waits for a msg
  c -- spawns --> d, c waits for a msg
  d --> waits for a message
  Once a message is received it will send the same message to the child
  and wait for another message until the number of messages reaches zero.

  ## Examples
  iex> Ring.MessengerNode.start_ring(3, 2, :hello)
  """
  def start_ring(num_nodes, num_msgs, message) do
    mpid = spawn(Ring.MessengerNode, :run, [nil, num_nodes, num_msgs, message])
    IO.puts "Spawn master #{inspect mpid} ---> sending :hello to #{inspect mpid}"
    send mpid, :hello
  end

  defmodule MessengerNode do

   def run(first_pid, num_nodes, num_msgs, message) 
    when first_pid == nil, do: run(self, num_nodes - 1, num_msgs, message)

    def run(first_pid, num_nodes, num_msgs, message) 
    when num_nodes > 0 do
      child_pid = spawn(__MODULE__, :run, [first_pid, num_nodes - 1, num_msgs, message])
      IO.puts "Spawned child #{inspect child_pid}"
      process(child_pid, num_msgs, message)
    end

    def run(first_pid, _num_nodes, num_msgs, message) do
      process(first_pid, num_msgs, message)
    end

    defp process(send_to, num_msgs, message) when num_msgs > 0 do
      receive do
        msg ->
          IO.puts "Received msg - my pid #{inspect self} - msg #{inspect msg}"
          IO.puts "Sending msg from #{inspect self} --> #{inspect send_to}"
          send(send_to, message)
          process(send_to, num_msgs - 1, msg)
      end
    end

    defp process(_send_to, _num_msgs, _message) do
      IO.puts "Ending #{inspect self}"
    end

  end
end
